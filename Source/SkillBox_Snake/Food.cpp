// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsFirst)
{
	ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
	if (Snake && bIsFirst)
	{
		if (Type == FoodType::SMALL)
		{
			Snake->AddSnakeElement();
		}

		if (Type == FoodType::BIG)
		{
			Snake->AddSnakeElement();
			Snake->AddSnakeElement();
		}

		if (Type == FoodType::SLOW_DOWN)
		{
			Snake->SlowDown();
		}

		Destroy();
	}
}

void AFood::SetType_Implementation(FoodType type)
{
	Type = type;
}
