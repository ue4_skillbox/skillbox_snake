// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkillBox_SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SKILLBOX_SNAKE_API ASkillBox_SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
