// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include <TimerManager.h>

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.3f;
	LastMovementDirection = MovementDirection::DOWN;
	SlowDownPeriod = 5.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	CreateSnake(4);
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement()
{
	FVector Vector = GetNewSnakeElementLocation();
	FTransform Transform(Vector);
	ASnakeElementBase* NewSnakeElenemt = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, Transform);
	NewSnakeElenemt->Snake = this;
	int Index = SnakeElemets.Add(NewSnakeElenemt);
	if (Index == 0)
	{
		NewSnakeElenemt->SetFirstElementType();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMovementDirection)
	{
		case MovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case MovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case MovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
		case MovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
		default:
			break;
	}

	SnakeElemets[0]->ToggleCollision();

	for (size_t i = SnakeElemets.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElemets[i];
		auto PrevElement = SnakeElemets[i-1];
		FVector NewLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(NewLocation);
	}

	SnakeElemets[0]->AddActorWorldOffset(MovementVector);
	SnakeElemets[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlapped(ASnakeElementBase* overlappedElement, AActor* OtherActor)
{
	if (IsValid(overlappedElement))
	{
		int index;
		SnakeElemets.Find(overlappedElement, index);
		bool bIsFirst = index == 0;
		IInteractable* Interactable = Cast<IInteractable>(OtherActor);
		if (Interactable)
		{
			Interactable->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SlowDown()
{
	float newSpeed = MovementSpeed * 2;
	SetActorTickInterval(newSpeed);
	FTimerHandle TimerHandle;
	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle,
		[&]() { this->SetActorTickInterval(MovementSpeed); }, SlowDownPeriod, false);
}

void ASnakeBase::CreateSnake(int ElementsAmount)
{
	for (size_t i = 0; i < ElementsAmount; i++)
	{
		AddSnakeElement();
	}
}

FVector ASnakeBase::GetNewSnakeElementLocation()
{
	if (SnakeElemets.Num() == 0)
	{
		return FVector(0, 0, 0);
	}

	ASnakeElementBase* LastSnakeElenemt = SnakeElemets.Last();
	FVector LastElemLocation = LastSnakeElenemt->GetActorLocation();
	switch (LastMovementDirection)
	{
	case MovementDirection::UP:
		LastElemLocation.X -= ElementSize;
		break;
	case MovementDirection::DOWN:
		LastElemLocation.X += ElementSize;
		break;
	case MovementDirection::LEFT:
		LastElemLocation.Y += ElementSize;
		break;
	case MovementDirection::RIGHT:
		LastElemLocation.Y -= ElementSize;
		break;
	}
	return LastElemLocation;
}

