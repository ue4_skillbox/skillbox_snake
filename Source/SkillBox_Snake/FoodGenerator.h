// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodGenerator.generated.h"

class AFood;

UCLASS()
class SKILLBOX_SNAKE_API AFoodGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodGenerator();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(BlueprintReadWrite)
	int InitialFoodAmount;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnFood();

	UFUNCTION()
	void FoodInstanceDestroyed(AActor* Actor);

private:
	int AlifeInstancesCount;

	void InitialSpawn();
};
