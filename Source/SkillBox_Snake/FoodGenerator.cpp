// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodGenerator.h"
#include "Food.h"

// Sets default values
AFoodGenerator::AFoodGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	InitialFoodAmount = 5;
}

// Called when the game starts or when spawned
void AFoodGenerator::BeginPlay()
{
	Super::BeginPlay();
	InitialSpawn();
}

// Called every frame
void AFoodGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFoodGenerator::SpawnFood()
{
	auto Location = FVector(FMath::RandRange(-400, 400), FMath::RandRange(-400, 400), 0);
	FTransform Transform(Location);
	AFood* foodInstance = GetWorld()->SpawnActor<AFood>(FoodClass, Transform);
	if (IsValid(foodInstance))
	{
		FoodType type = (FoodType)FMath::RandRange(0, 2);
		foodInstance->SetType(type);
		foodInstance->OnDestroyed.AddDynamic(this, &AFoodGenerator::FoodInstanceDestroyed);
	}
	AlifeInstancesCount++;
}

void AFoodGenerator::FoodInstanceDestroyed(AActor* Actor)
{
	AlifeInstancesCount--;
	if (AlifeInstancesCount <= 0)
	{
		SpawnFood();
	}
}

void AFoodGenerator::InitialSpawn()
{
	for (size_t i = 0; i < InitialFoodAmount; i++)
	{
		SpawnFood();
	}
}