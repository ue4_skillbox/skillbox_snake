// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class SkillBox_SnakeEditorTarget : TargetRules
{
	public SkillBox_SnakeEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "SkillBox_Snake" } );
	}
}
